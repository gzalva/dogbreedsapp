import { createSlice } from "@reduxjs/toolkit";

export const dogSlice = createSlice({
    name: "dog",
    initialState: {},
    reducers: {
        setDog: (state, action) => {
            const dog = action.payload;
            state.image = dog.image;
            state.breed = dog.breed;
        }
    }
});

export const {
    setDog
} = dogSlice.actions;
