import { createSlice } from "@reduxjs/toolkit";

export const breedsSlice = createSlice({
    name: "breeds",
    initialState: [],
    reducers: {
        setBreeds: (state, action) => {
            state.splice(0, state.length);
            state.push(...action.payload);
        }
    }
});

export const {
    setBreeds
} = breedsSlice.actions;
