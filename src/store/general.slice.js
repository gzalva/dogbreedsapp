import { createSlice } from "@reduxjs/toolkit";

export const generalSlice = createSlice({
    name: "loading",
    initialState: {
        isLoading: true
    },
    reducers: {
        setLoading: (state, action) => {
            state.isLoading = action.payload;
        }
    }
});

export const {
    setLoading
} = generalSlice.actions;
