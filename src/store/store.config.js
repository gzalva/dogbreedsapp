import { configureStore } from "@reduxjs/toolkit";
import { breedsSlice } from "./breeds.slice";
import { generalSlice } from "./general.slice";
import { dogSlice } from "./dog.slice";

const reducer = {
    general: generalSlice.reducer,
    breeds: breedsSlice.reducer,
    dog: dogSlice.reducer
};

const store = configureStore({ reducer });

export default store;
