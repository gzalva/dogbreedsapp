import Container from '@material-ui/core/Container';
import LinearProgress from '@material-ui/core/LinearProgress';
import React, { useEffect, useRef } from 'react';
import CardComponent from './components/CardComponent/CardComponent';
import SelectComponent from './components/SelectComponent/SelectComponent';
import { useApi } from "./hooks/api.hook";
import { useAppSelector } from "./hooks/store.hook";

const App = () => {

    const isLoading = useAppSelector(state => state["general"].isLoading);

    const loadDogRef = useRef(useApi().loadDog);

    useEffect(() => {
        loadDogRef.current();
    }, []);

    return (
        <Container maxWidth="sm">
            <SelectComponent />
            {
                isLoading ?
                    <LinearProgress /> :
                    <CardComponent />
            }
        </Container>
    );
};

export default App;
