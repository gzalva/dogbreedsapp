export class AppConfig {

    static API_URL = "https://api.thedogapi.com/v1";
    static API_URL_BREEDS = `${ AppConfig.API_URL }/breeds`;
    static API_URL_SEARCH = `${ AppConfig.API_URL }/images/search`;
    static API_URL_SEARCH_BY_BREED_IDS = `${ AppConfig.API_URL }/images/search?breed_ids=`;

    static RANDOM_BREED = {
        id: 0,
        name: "random"
    };
}
