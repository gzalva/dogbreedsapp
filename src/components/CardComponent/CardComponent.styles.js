import { makeStyles } from "@material-ui/core/styles";

export const useCardComponentStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 600,
        boxShadow: '1px 2px 10px rgba(7,7,7, .2)',
        transition: 'transform .1s',
        "&:hover": {
            transform: 'scale(1.025)'
        }
    },
    media: {
        height: 480,
    }
}));
