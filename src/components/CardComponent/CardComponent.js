import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { useApi } from "../../hooks/api.hook";
import { useAppSelector } from "../../hooks/store.hook";
import { useCardComponentStyles } from "./CardComponent.styles";

const CardComponent = () => {

    const dog = useAppSelector(state => state["dog"]);
    const classes = useCardComponentStyles();

    const { loadDog } = useApi();

    return (
        <Container maxWidth="sm">
            {
                dog &&
                <Card className={ classes.root } onClick={ () => loadDog(dog.breed.id) }>
                    <CardActionArea>
                        {
                            dog.image &&
                            <CardMedia className={ classes.media } image={ dog.image } title={ dog.breed.name } />
                        }
                    </CardActionArea>
                    <CardContent>
                        <Typography variant="h4" align="center">{ dog.breed.name }</Typography>
                    </CardContent>
                </Card>
            }
        </Container>
    );
};

export default CardComponent;
