import { makeStyles } from "@material-ui/core/styles";

export const useSelectComponentStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 500,
        boxShadow: '1px 2px 10px rgba(7,7,7, .2)',
        transition: 'transform .1s',
        "&:hover": {
            transform: 'scale(1.025)'
        }
    },
    formControl: {
        margin: theme.spacing(4),
        minWidth: 300,
    },
    media: {
        height: 280,
    }
}));
