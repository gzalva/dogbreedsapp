import Container from '@material-ui/core/Container';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import React, { useEffect, useRef } from 'react';
import { useApi } from "../../hooks/api.hook";
import { useAppSelector } from "../../hooks/store.hook";
import { useSelectComponentStyles } from "./SelectComponent.styles";

const SelectComponent = () => {

    const breeds = useAppSelector(state => state["breeds"]);
    const classes = useSelectComponentStyles();

    const loadBreedsRef = useRef(useApi().loadBreeds);
    const { loadDog } = useApi();

    useEffect(() => {
        loadBreedsRef.current();
    }, []);

    return (
        <Container maxWidth="sm">
            <Grid container justifyContent="center">
                <FormControl variant="outlined" className={ classes.formControl }>
                    <InputLabel id="select-outlined-label">Most popular dog breeds</InputLabel>
                    <Select labelId="select-outlined-label" id="breed-select" label="Most popular dog breeds" onChange={ (e) => loadDog(e.target.value) }>
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        {
                            breeds.map(breed => (
                                <MenuItem key={ breed.id } value={ breed.id }>{ breed.name }</MenuItem>
                            ))
                        }
                    </Select>
                </FormControl>
            </Grid>
        </Container>
    );
};

export default SelectComponent;
