import { AppConfig } from "../app.config";
import { setBreeds } from "../store/breeds.slice";
import { setDog } from "../store/dog.slice";
import { setLoading } from "../store/general.slice";
import { useLogger } from "./logger.hook";
import { useAppDispatch } from "./store.hook";

export const useApi = () => {

    const log = useLogger("useApi");
    const dispatch = useAppDispatch();

    const loadBreeds = async () => {
        const res = await fetchUrl(AppConfig.API_URL_BREEDS);
        const breeds = await res.json();

        dispatch(setBreeds(breeds));

        log.d("Loaded breeds:", breeds);
    };

    const loadDog = async (breedId) => {
        dispatch(setLoading(true));

        const url = breedId ?
            `${ AppConfig.API_URL_SEARCH_BY_BREED_IDS }${ breedId }` :
            AppConfig.API_URL_SEARCH;

        const res = await fetchUrl(url);
        const [data] = await res.json();

        let {
            url: image,
            breeds: [breed]
        } = data;

        if (!breed) {
            breed = AppConfig.RANDOM_BREED;
        }

        const dog = {
            image: image,
            breed: breed
        };

        dispatch(setDog(dog));

        log.d("Loaded dog:", dog);

        dispatch(setLoading(false));
    };

    const fetchUrl = async (url) => {
        const res = await fetch(url);

        if (!res.ok) {
            throw Error(`Error fetching from url: ${ url }`);
        }

        return res;
    };

    return {
        loadBreeds,
        loadDog
    };
};
