export const useLogger = (name) => {

    const d = (message, ...vars) => {
        console.log(`[${ name }]: ${ message }`, vars);
    };

    const w = (message, ...vars) => {
        console.warn(`[${ name }]: ${ message }`, vars);
    };

    const e = (message, ...vars) => {
        console.error(`[${ name }]: ${ message }`, vars);
    };

    return {
        d,
        w,
        e
    };
};
